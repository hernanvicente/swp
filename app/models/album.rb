class Album < ApplicationRecord
  belongs_to :artist
  has_many :songs

  validates :name, :path, presence: true
end
