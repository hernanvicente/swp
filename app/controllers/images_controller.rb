# frozen_string_literal: true

class ImagesController < ApplicationController
  def create
    artist = Artist.find(params[:artist_id])
    image = params[:image].present? ? params[:image] : nil
    artist.update(image: image)
  end
end
