# frozen_string_literal: true

class SongsController < ApplicationController
  def index
    @songs = Song.includes(album: [:artist])
  end
end
