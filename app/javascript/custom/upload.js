import axios from 'axios';

export default function (url, file = '', name = 'image') {
  if (typeof url !== 'string') {
    throw new TypeError(`Expected a string, got ${typeof url}`);
  }

  const token = document.querySelector('meta[name="csrf-token"]').getAttribute('content')
  const formData = new FormData();

  formData.append(name, file);
  formData.append('authenticity_token', token);

  const config = {
    headers: {
      'content-type': 'multipart/form-data'
    }
  };

  return axios.post(url, formData, config);
};