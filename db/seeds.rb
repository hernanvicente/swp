# frozen_string_literal: true

# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

# MUSIC_ROOT_PATH = '/Volumes/hernan2/Music/iTunes Media/Music'
SYSTEM_FILES = ['.iTunes Preferences.plist', '.DS_Store',
                '.com.apple.timemachine.supported'].freeze

# Save artists
if Artist.count.zero?
  artists = Dir.children('/music') - SYSTEM_FILES
  artists.each do |artist|
    Artist.create(name: artist, path: artist)
  end
end

# Save albums
if Artist.count.positive? && Album.count.zero?
  Artist.all.each do |artist|
    albums = Dir.children("/music/#{artist.path}/") - SYSTEM_FILES
    albums.each do |album|
      Album.create(name: album, path: album, artist: artist)
    end
  end
end

# Save songs
if Album.count.positive? && Song.count.zero?
  Album.all.each do |album|
    songs = Dir.children("/music/#{album.artist.path}/#{album.path}") - SYSTEM_FILES
    songs.each do |song|
      Song.create(name: song, file_name: song, album: album)
    end
  end
end
