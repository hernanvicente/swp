# frozen_string_literal: true

Rails.application.routes.draw do
  get 'pages/index'
  root 'pages#index'

  resources :albums, only: %i[index show]
  resources :artists, only: %i[index show] do
    resource :image, only: %i[create]
  end
  resources :songs, only: %i[index]
end
