# frozen_string_literal: true

require 'test_helper'

class AlbumTest < ActiveSupport::TestCase
  should belong_to(:artist)
  should have_many(:songs)
  should validate_presence_of(:name)
  should validate_presence_of(:path)
end
