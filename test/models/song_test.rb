# frozen_string_literal: true

require 'test_helper'

class SongTest < ActiveSupport::TestCase
  should belong_to(:album)
  should validate_presence_of(:name)
  should validate_presence_of(:file_name)
end
