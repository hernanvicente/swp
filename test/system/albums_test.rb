# frozen_string_literal: true

require 'application_system_test_case'

class AlbumsTest < ApplicationSystemTestCase
  test 'visiting the show' do
    album = create(:album)
    songs = create_list(:song, 3, album: album)
    visit album_url(album)

    assert_selector 'h1', text: album.name
    assert_selector 'h4', text: "by #{album.artist.name}"
    songs.each do |song|
      assert_selector 'td', text: song.name
    end
  end
end
