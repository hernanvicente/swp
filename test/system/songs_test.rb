# frozen_string_literal: true

require 'application_system_test_case'

class SongsTest < ApplicationSystemTestCase
  test 'visiting the index' do
    songs = create_list(:song, 5)
    visit songs_url

    assert_selector 'h1', text: '5 Songs'
    songs.each do |song|
      assert_selector 'td', text: song.name
      assert_selector 'td', text: song.album.name
      assert_selector 'td', text: song.album.artist.name
    end
  end
end
