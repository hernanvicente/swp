# frozen_string_literal: true

require 'application_system_test_case'

class ArtistsTest < ApplicationSystemTestCase
  test 'visiting the index' do
    visit artists_url
    assert_selector 'h1', text: 'Artists'
  end

  test 'visiting the index with artist' do
    artist = create_list(:artist, 2)
    visit artists_url
    assert_selector 'h1', text: '2 Artists'
    artist.each do |artist|
      assert_selector '.card', text: artist.name
    end
  end

  test 'visiting the show' do
    artist = create(:artist)
    albums = create_list(:album, 3, artist: artist)
    visit artist_url(artist)
    assert_selector 'h1', text: artist.name
    albums.each do |album|
      assert_selector '.card', text: album.name
    end
  end
end
