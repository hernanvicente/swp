# frozen_string_literal: true

require 'test_helper'

class ApplicationSystemTestCase < ActionDispatch::SystemTestCase
  TARGET_RESOLUTION = [1366, 768].freeze
  SWITCHES = %W[--disable-dev-shm-usage
                --window-size=#{TARGET_RESOLUTION[0]},#{TARGET_RESOLUTION[1]}
                --disable-translate].freeze
  OPTIONS = { 'args' => SWITCHES }.freeze

  Capybara.server = :puma, { Silent: true }
  Capybara.register_driver :selenium_remote do |app|
    capabilities = Selenium::WebDriver::Remote::Capabilities.chrome(
      chromeOptions: OPTIONS
    )
    client = Selenium::WebDriver::Remote::Http::Default.new
    opts = {
      browser: :remote,
      url: ENV['SELENIUM_REMOTE_URL'],
      desired_capabilities: capabilities,
      http_client: client
    }
    Capybara::Selenium::Driver.new(app, opts)
  end

  driven_by :selenium_remote

  def setup
    Capybara.current_driver = :selenium_remote
    Capybara.javascript_driver = :selenium_remote
    Capybara.server_host = '0.0.0.0'
    ip = Socket.ip_address_list.detect(&:ipv4_private?).ip_address
    host! "http://#{ip}:#{Capybara.server_port}"
  end
end
