FactoryBot.define do
  factory :album do
    name { FFaker::Name.name }
    path { FFaker::Name.name }
    artist
  end
end
