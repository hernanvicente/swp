# frozen_string_literal: true

FactoryBot.define do
  factory :song do
    name      { FFaker::Name.name }
    file_name { FFaker::Name.name }
    album
  end
end
