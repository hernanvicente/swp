# frozen_string_literal: true

FactoryBot.define do
  factory :artist do
    name  { FFaker::Name.name }
    path  { FFaker::Name.name }

    trait :with_image do
      after(:build) do |artist|
        artist.image.attach(
          content_type: 'image/png',
          filename: 'image.png',
          io: File.open(Rails.root.join('test', 'fixtures', 'files', 'image.png'))
        )
      end
    end
  end
end
