# frozen_string_literal: true

require 'test_helper'

class AlbumsControllerTest < ActionDispatch::IntegrationTest
  test 'should get index' do
    get albums_url
    assert_response :success
  end

  test 'should get show' do
    album = create(:album)
    get album_url(album)
    assert_response :success
  end
end
