# frozen_string_literal: true

require 'test_helper'

class ArtistsControllerTest < ActionDispatch::IntegrationTest
  test 'should get index' do
    get artists_url
    assert_response :success
  end

  test 'should get show' do
    artist = create(:artist)
    get artist_url(artist)
    assert_response :success
  end
end
