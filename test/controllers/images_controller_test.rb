# frozen_string_literal: true

require 'test_helper'

class ImagesControllerTest < ActionDispatch::IntegrationTest
  test 'create action with image for artist without image' do
    artist = create(:artist)
    file_path = Rails.root.join('test', 'fixtures', 'files', 'image.png')
    image = fixture_file_upload(file_path, 'image/png')
    post artist_image_url(artist), params: { image: image, name: 'image' }
    assert_response :success
  end

  test 'create action with image for artist image exist' do
    artist = create(:artist, :with_image)
    file_path = Rails.root.join('test', 'fixtures', 'files', 'image.png')
    image = fixture_file_upload(file_path, 'image/png')
    post artist_image_url(artist), params: { image: image }
    assert_response :success
  end

  test 'create action without image for artist with image' do
    artist = create(:artist, :with_image)
    post artist_image_url(artist), params: { image: '', name: 'image' }
    assert_response :success
  end
end
