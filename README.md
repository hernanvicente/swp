# SWP - Single Web Player
---
This README would normally document whatever steps are necessary to get the
application up and running.

* Ruby version: 2.6.5

* Rails version: 6.0.2

* Bulma CSS version: 0.8.0

* System dependencies:
  - Docker
  - Docker Compose

### Start the application

Update your environment variables:
1. Copy `.env.example` to `.env`
2. Set `HOST_MUSIC_PATH` value with the path directory of your host. E.g `/Users/Music/iTunes Media/Music` or `C:\Home\Users\MyUsers\Music`

Run your application from the terminal console:
```
docker-compose -f docker-compose.yml -f docker-compose.development.yml up -d
```
Database creation
```
docker exec -it swp_app_1 bundle exec rails db:create
```

Database initialization
```
docker exec -it swp_app_1 bundle exec rails db:seed
```

* Services (job queues, cache servers, search engines, etc.)

* Deployment instructions

Run your tests without ruby 2.7 warning messages:

```
docker exec -it -e RUBYOPT='-W:no-deprecated' swp_test_1 bundle exec rake
```
