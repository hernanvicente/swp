#!/bin/bash
set -e

bundle exec rails db:migrate
bundle exec rails db:seed

bundle exec puma -C config/puma.rb